================
Module Reference
================


One dimensional version
=======================

.. automodule:: swe2hs.one_dimensional
.. currentmodule:: swe2hs.one_dimensional

.. autosummary::
   :toctree: _autosummary

   convert_1d

Two dimensional distributed version
===================================

.. automodule:: swe2hs.two_dimensional
.. currentmodule:: swe2hs.two_dimensional

.. autosummary::
   :toctree: _autosummary

   convert_2d

Two dimensional step-by-step version
====================================

.. automodule:: swe2hs.stepwise
.. currentmodule:: swe2hs.stepwise

.. autosummary::
   :toctree: _autosummary

   process_timestep_from_nc_files

Visualization utils
===================

.. automodule:: swe2hs.visualization
.. currentmodule:: swe2hs.visualization

.. autosummary::
   :toctree: _autosummary

   groupby_hydroyear
   layer_plot

Datetime and Gap Detection Utils
================================

.. automodule:: swe2hs.utils
.. currentmodule:: swe2hs.utils

.. autosummary::
   :toctree: _autosummary

   continuous_timedeltas
   continuous_timedeltas_in_nonzero_chunks
   fill_small_gaps
   get_nonzero_chunk_idxs
   get_small_gap_idxs
   get_zeropadded_gap_idxs
