SWE2HS Documentation
====================

.. image:: https://img.shields.io/gitlab/pipeline-status/aschauer/swe2hs?branch=master&gitlab_url=https%3A%2F%2Fgitlabext.wsl.ch&label=Pipeline Status
   :alt: Gitlab pipeline status (self-hosted)
   :target: https://code.wsl.ch/aschauer/swe2hs/-/commits/master

.. image:: https://img.shields.io/pypi/v/swe2hs.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/swe2hs/

.. image:: https://img.shields.io/pypi/pyversions/swe2hs
    :alt: PyPI - Python Version

|

This is the documentation of the ``swe2hs`` Python package.

.. include:: ../README.rst
   :start-after: .. start_intro
   :end-before: .. end_intro

|

.. image:: _static/colored_layers_kuhtai_2002.png
   :alt: Schematic snowpack evolution

|

.. include:: ../README.rst
   :start-after: .. start_figure_caption
   :end-before: .. end_figure_caption

|

Contents
========

.. toctree::
   :maxdepth: 2

   Getting started <getting_started>
   Examples <examples>
   Contributing <contributing>
   Module Reference <api>
   Changelog <changelog>

|

.. include:: ../README.rst
   :start-after: .. start_bib
   :end-before: .. end_bib

