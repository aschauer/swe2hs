﻿swe2hs.utils.get\_nonzero\_chunk\_idxs
======================================

.. currentmodule:: swe2hs.utils

.. autofunction:: get_nonzero_chunk_idxs