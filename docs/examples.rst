==================
Examples and Usage
==================

In the following you find notebooks which demonstrate how the :mod:`swe2hs` 
Pathon package can be used. Basic knowledge of :mod:`pandas` 
and :mod:`xarray` is expected for the examples.

.. toctree::
    :maxdepth: 1

    examples/transfer_one_dimensional_series
    examples/transfer_two_dimensional_dataarray


