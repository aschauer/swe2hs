# This file is used to configure your project.
# Read more about the various options under:
# https://setuptools.pypa.io/en/latest/userguide/declarative_config.html
# https://setuptools.pypa.io/en/latest/references/keywords.html

[metadata]
name = swe2hs
description = Conceptual snow density model for transferring snow water equivalent to snow depth.
author = Johannes Aschauer
author_email = johannes.aschauer@slf.ch
license = GPL-3.0-or-later
license_files = LICENSE.txt
long_description = file: README.rst
long_description_content_type = text/x-rst; charset=UTF-8
url = https://code.wsl.ch/aschauer/swe2hs
project_urls =
    Documentation = https://aschauer.gitlab-pages.wsl.ch/swe2hs/
    Changelog = https://aschauer.gitlab-pages.wsl.ch/swe2hs/changelog.html
    Tracker = https://code.wsl.ch/aschauer/swe2hs/-/issues

# Change if running only on Windows, Mac or Linux (comma-separated)
platforms = any

# Add here all kinds of additional classifiers as defined under
# https://pypi.org/classifiers/
classifiers =
    Development Status :: 4 - Beta
    Programming Language :: Python
    Programming Language :: Python :: 3.7
    Programming Language :: Python :: 3.8
    Programming Language :: Python :: 3.9
    Intended Audience :: Science/Research
    License :: OSI Approved :: GNU General Public License v3 (GPLv3)
    Topic :: Scientific/Engineering :: Hydrology
    Topic :: Scientific/Engineering :: Physics


[options]
zip_safe = False
packages = find_namespace:
include_package_data = True
package_dir =
    =src

# Require a min/specific Python version (comma-separated conditions)
# python_requires = >=3.8

# Add here dependencies of your project (line-separated), e.g. requests>=2.2,<3.0.
# Version specifiers like >=2.2,<3.0 avoid problems due to API changes in
# new major versions. This works if the required packages follow Semantic Versioning.
# For more information, check out https://semver.org/.
install_requires =
    importlib-metadata; python_version<"3.8"
    pandas>=1.3
    netcdf4>=1.5.8
    numpy>=1.18
    numba>=0.55.1
    xarray[parallel]>=0.19
    matplotlib>=3.3


[options.packages.find]
where = src
exclude =
    tests

[options.extras_require]
# Add here additional requirements for extra features, to install with:
# `pip install swe2hs[PDF]` like:
# PDF = ReportLab; RXP

# Add here test requirements (semicolon/line-separated)
testing =
    setuptools
    pytest
    pytest-cov

[options.entry_points]
# Add here console scripts like:
# console_scripts =
#     script_name = swe2hs.module:function

[tool:pytest]
# Specify command line options as you would do when invoking pytest directly.
# e.g. --cov-report html (or xml) for html/xml output or --junitxml junit.xml
# in order to write a coverage file that can be read by Jenkins.
# CAUTION: --cov flags may prohibit setting breakpoints while debugging.
#          Comment those flags to avoid this pytest issue.
addopts =
    --cov swe2hs --cov-report term-missing
    --verbose
norecursedirs =
    dist
    build
    .tox
testpaths = tests
# Use pytest markers to select/deselect specific tests
# markers =
#     slow: mark tests as slow (deselect with '-m "not slow"')
#     system: mark end-to-end system tests

[devpi:upload]
# Options for the devpi: PyPI server and packaging tool
# VCS export must be deactivated since we are using setuptools-scm
no_vcs = 1
formats = bdist_wheel

[flake8]
# Some sane defaults for the code style checker flake8
max_line_length = 88
extend_ignore = E203, W503
# ^  Black-compatible
#    E203 and W503 have edge cases handled by black
exclude =
    .tox
    build
    dist
    .eggs
    docs/conf.py

[pyscaffold]
# PyScaffold's parameters when the project was created.
# This will be used when updating. Do not change!
version = 4.1.4
package = swe2hs
